# Blog Code/CSDN/3/Linux

([简体中文](README.md) | English)

#### Description
This folder contains code of the Linux version of streaming ASR with real-time
recording shared in the third blog of my blog column 【分享本周所学】.

#### Installation
1. Clone or download this repository.
2. Copy [module.py](module.py) to
   /home/rfdragon/.paddlehub/u2_conformer_wenetspeech to replace the existing
   files. (This is the default folder. Remember to change "rfdragon" into your
   own username. .paddlehub is a hidden folder. Try enabling "Show Hidden
   Files" if you cannot find it.)
3. Copy the [paddlespeech](paddlespeech) folder to
   /home/rfdragon/.local/lib/python3.8/site-packages to replace the existing
   files. (Remember to change "rfdragon" into your own username and "python3.8"
   into your Python version. .local is a hidden folder.)
4. Enjoy streaming ASR with real-time recording by running
   [Speech Recognition.py](Speech%20Recognition.py)!

If you encounter any problem while installing, please see
https://blog.csdn.net/weixin_48978134/article/details/125686296?spm=1001.2014.3001.5501
for details.