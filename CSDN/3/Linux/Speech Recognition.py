import numpy as np
import paddlehub as hub
import pyaudio

pa = pyaudio.PyAudio()
model = hub.Module(name='u2_conformer_wenetspeech', version='1.0.0')
# The following statement preloads the model by letting it recognize some
# random things.
model.speech_recognize((np.zeros((8000, 1), dtype='float32'), 16000),
                       device='gpu:0',
                       from_file=False)
# The model requires the sampling rate of the input audio to be 16000.
# Each audio segment recorded will last 2s.
stream = pa.open(16000,
                 1,
                 pyaudio.paFloat32,
                 input=True,
                 frames_per_buffer=32000)
print("Start recording...")
wav = b''  # This generates an empty bytes object.
while True:
    # Bytes objects can be concatenated with the operation "+" like strings.
    wav += stream.read(32000)
    audio = np.frombuffer(wav, dtype='float32')
    # The input array of the model must be 2-dimensional.
    audio = np.expand_dims(audio, -1)
    text = model.speech_recognize((audio, 16000),
                                  device='gpu:0',
                                  from_file=False)
    print(text)