# Blog Code/CSDN/3/Linux

（简体中文 | [English](README.en.md)）

#### 介绍
本文件夹包含博客专栏【分享本周所学】第三期内分享的Linux上的实时录音流式语音识别代码。

#### 安装
1. 将仓库克隆或下载到本地。
2. 将[module.py](module.py)复制到/home/rfdragon/.paddlehub/u2_conformer_wenetspeech
   （这是默认路径，记得把rfdragon改成你自己的用户名，.paddlehub是隐藏文件夹，如果找不到可以试试启用显示隐藏文件夹）替换原有文件。
3. 将[paddlespeech](paddlespeech)文件夹复制到
   /home/rfdragon/.local/lib/python3.8/site-packages（记得把rfdragon改成你自己的用户名，把
   python3.8改成你自己的python版本；.local是隐藏文件夹）替换原有文件。
4. 运行[Speech Recognition.py](Speech%20Recognition.py)，即可实现实时录音的流式语音识别。

如安装时遇到问题，请见
https://blog.csdn.net/weixin_48978134/article/details/125686296?spm=1001.2014.3001.5501。