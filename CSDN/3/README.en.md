# Blog Code/CSDN/3

([简体中文](README.md) | English)

#### Description
This folder contains code shared in the third blog of my blog column 【分享本周所学】.

#### Installation
To use the Linux version, please open the [Linux](Linux) folder and read
[README.en.md](Linux/README.en.md) inside.
To use the Windows version, please open the [Windows](Windows) folder and read
[README.en.md](Windows/README.en.md) inside.

If you encounter any problem while installing, please see
https://blog.csdn.net/weixin_48978134/article/details/125686296 for details.