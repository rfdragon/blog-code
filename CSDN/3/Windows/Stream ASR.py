from paddlespeech.server.bin.paddlespeech_client import ASROnlineClientExecutor

asr = ASROnlineClientExecutor()
print('-' * 30)
asr(None,
    port=8090,
    from_file=False,
    recording=True) 