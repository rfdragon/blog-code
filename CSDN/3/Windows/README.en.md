# Blog Code/CSDN/3/Windows

([简体中文](README.md) | English)

#### Description
This folder contains code of the Windows version of streaming ASR with
real-time recording shared in the third blog of my blog column 【分享本周所学】.

#### Installation
1. Clone or download this repository.
2. Enter the folder where you installed Python and open Lib\site-packages. Copy
   the [paddlespeech](paddlespeech) folder into it to replace the existing
   files.
3. Enjoy streaming ASR with real-time recording by running
   [Stream ASR.py](Stream%20ASR.py)!

If you encounter any problem while installing, please see
https://blog.csdn.net/weixin_48978134/article/details/125686296?spm=1001.2014.3001.5501
for details.