# Blog Code/CSDN/3/Windows

（简体中文 | [English](README.en.md)）

#### 介绍
本文件夹包含博客专栏【分享本周所学】第三期内分享的Windows上的实时录音流式语音识别代码。

#### 安装
1. 将仓库克隆或下载到本地。
2. 进入到你安装Python的目录下，打开Lib\site-packages，并把[paddlespeech](paddlespeech)文件夹复制进去替换
   原有文件。
3. 运行[Stream ASR.py](Stream%20ASR.py)，即可实现实时录音的流式语音识别。

如安装时遇到问题，请见
https://blog.csdn.net/weixin_48978134/article/details/125686296?spm=1001.2014.3001.5501。