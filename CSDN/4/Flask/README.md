# Blog Code/CSDN/4/Flask

（简体中文 | [English](README.en.md)）

#### 介绍
本文件夹包含博客专栏【分享本周所学】第四期内分享的使用Flask实现AI流式语音识别服务化的代码。

#### 安装
建议在Windows上进行安装。

1. 将仓库克隆或下载到本地。
2. 前往[CUDA Toolkit Archive](https://developer.nvidia.com/cuda-toolkit-archive)下
   载并安装CUDA。
3. 前往[CUDA深度神经网络库](https://developer.nvidia.cn/zh-cn/cudnn)下载并安装CUDNN。
4. 前往[飞桨PaddlePaddle](https://www.paddlepaddle.org.cn/)根据你的CUDA版本安装
   PaddlePaddle。
5. 前往
   [Archieved: Python Extension Packages for Windows](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio)
   下载并安装PyAudio。
6. 在命令行中使用以下命令安装PaddleSpeech、flask和requests：
```shell
python -m pip install paddlespeech flask requests -U -i https://pypi.tuna.tsinghua.edu.cn/simple
```
7. 运行[Streaming ASR Server.py](Streaming%20ASR%20Server.py)，启动流式语音识别服务器。
8. 运行[Streaming ASR Client.py](Streaming%20ASR%20Client.py)，即可实现实时录音的流式语音识别。

如安装时遇到问题，请见
https://blog.csdn.net/weixin_48978134/article/details/125770821?spm=1001.2014.3001.5501。