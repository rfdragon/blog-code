import pyaudio
import requests

pa = pyaudio.PyAudio()
stream = pa.open(16000,
                 1,
                 pyaudio.paInt16,
                 input=True,
                 frames_per_buffer=32000)
while True:
    wav = stream.read(32000)
    r = requests.post('http://127.0.0.1:5000/asr', data=wav)
    print(r.text)