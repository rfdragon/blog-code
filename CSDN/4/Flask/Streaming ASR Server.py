from flask import Flask, request
from paddlespeech.server.utils.config import get_config
from paddlespeech.server.engine.asr.online.asr_engine import PaddleASRConnectionHanddler
from paddlespeech.server.engine.engine_pool import get_engine_pool, init_engine_pool


class Server(PaddleASRConnectionHanddler):

    def __init__(self, path):
        config = get_config(path)
        init_engine_pool(config)
        engine = get_engine_pool()['asr']
        super(Server, self).__init__(engine)

    def execute(self, wav):
        self.extract_feat(wav)
        self.decode()
        text = self.get_result()
        return text


path = r'.\ws_conformer_wenetspeech_application.yaml'
server = Server(path)
app = Flask(__name__)


@app.route('/asr', methods=['GET', 'POST'])
def asr():
    if request.method == 'POST':
        wav = request.stream.read()
        text = server.execute(wav)
        return text


if __name__ == "__main__":
    app.run()