# Blog Code/CSDN/4/Flask

([简体中文](README.md) | English)

#### Description
This folder contains code of Real-time streaming ASR server shared in the
fourth blog of my blog column 【分享本周所学】.

#### Installation
Installing on Windows is recommended.

1. Clone or download this repository.
2. Download and install CUDA from
   [CUDA Toolkit Archive](https://developer.nvidia.com/cuda-toolkit-archive).
3. Download and install CUDNN from
   [cuDNN Download](https://developer.nvidia.cn/rdp/cudnn-download).
4. Install PaddlePaddle according to your CUDA version from
   [PaddlePaddle](https://www.paddlepaddle.org.cn/en).
5. Download and install PyAudio from
   [Archieved: Python Extension Packages for Windows](https://www.lfd.uci.edu/~gohlke/pythonlibs/#pyaudio).
6. Use terminal to install PaddleSpeech, flask and requests with the following
   command:
```shell
python -m pip install paddlespeech -U -i https://pypi.tuna.tsinghua.edu.cn/simple
```
7. Start the [Streaming ASR Server](Streaming%20ASR%20Server.py).
7. Enjoy streaming ASR with real-time voice recording by running
   [Streaming ASR Client.py](Streaming%20ASR%20Client.py)!

If you encounter any problem while installing, please see
https://blog.csdn.net/weixin_48978134/article/details/125770821?spm=1001.2014.3001.5501
for details.